<html lang = "en">
    <head>
        <meta charset="uft-8">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/charts.css" type="text/css">
        <link rel="stylesheet" href="../css/CarouselSlider.css" type="text/css">

        <?php
        include("connect_to_db.php"); 
        $lvls_down = 1;
        $lvls_up = 1;
        $id = 1;
        
        // $id = "@R1@";

        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }

        if (isset($_GET["lvls_down"])) {
            $lvls_down = $_GET["lvls_down"];
        }

        if (isset($_GET["lvls_up"])) {
            $lvls_up = $_GET["lvls_up"];
        }

        //Connecting to the database, TODO change this so when this is on a server and not on a local host so this cant be seen
        $mysqli = connect_to_db("../config.ini");
        if ($mysqli -> connect_errno) {
            echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
            exit();
        }

        function find_parents($id, $mysqli, $father = TRUE) {
            // This function returns the fathers id of the given person if $father is True else it will retrun their mother, 
            // if the given person doesnt have a parrent in the database, it will return false
            if ($father) {
                $gender = "male";
            } else {
                $gender = "female";
            }

            $query = 
            "SELECT individual_1_db_id 
                FROM relationships
                    INNER JOIN individuals
                        ON individuals.id = relationships.individual_1_db_id
                            WHERE individual_2_db_id = '". $id. "' AND relationship_type='parental' AND gender = '". $gender. "'";
            $result = $mysqli -> query($query);

            if ($result->num_rows > 0) {
                while ($row = $result -> fetch_assoc()) {
                    $parent = $row["individual_1_db_id"];
                }
                return $parent;
            } else {
                return FALSE;
            }
        }

        function find_married($id, $mysqli) {
            $query = 
            "SELECT individual_1_db_id 
                FROM relationships
                    INNER JOIN individuals
                        ON individuals.id = relationships.individual_1_db_id
                            WHERE individual_2_db_id = '". $id. "' AND relationship_type='marriage'";

            $result = $mysqli -> query($query);

            $spouses = array();
            if ($result->num_rows > 0) {
                while ($row = $result -> fetch_assoc()) {
                    array_push($spouses, $row["individual_1_db_id"]);

                }
                return $spouses;
            } else {
                return FALSE;
            }

        }

        function find_children($id, $mysqli) {
            $query = 
            "SELECT individual_2_db_id
                FROM relationships
                    INNER JOIN individuals 
                        ON individuals.id = relationships.individual_2_db_id
                            WHERE individual_1_db_id = '". $id. "' AND relationship_type = 'parental'
                                ORDER BY birth_year";
            $result = $mysqli -> query($query);

            $children = array();
            if ($result->num_rows > 0) {
                while ($row = $result -> fetch_assoc()) {
                    array_push($children, $row["individual_2_db_id"]);
                }
                return $children;
            } else {
                return FALSE;
            }


        }

        function find_top_in_chain($id, $mysqli, $father = True, $max = 200) {
            //
            $counter = 0;
            $new_id = $id;

            while ($counter <= $max and $new_id){
                $id = $new_id;
                $new_id = find_parents($id, $mysqli, $father);
                $counter = $counter + 1;
            }

            return $id;

        }

        function print_self($id, $spouses, $mysqli, $target) {
            $text_format = 
                "
                <a target='_parent' href='%s'>
                    <span class='%s text-dark text-justify text-truncate content'>
                        %s %s %s </br>
                        * %s</br>
                        + %s
                    </span>
                </a>                         
                ";
            
            $format_query = 
            "SELECT * 
                FROM Individuals
                    WHERE id='%s'";

            $self_data = $mysqli -> query(sprintf($format_query, $id));

            echo "<div>";
            if ($self_data->num_rows > 0) {
                while($row = $self_data->fetch_assoc()) {
                    if ($target == $id) {
                        echo sprintf(
                            $text_format,
                            "#main",
                            $row["gender"]. " target", 
                            "<b>". $row["first_name"], $row["insertion"], $row["last_name"],
                            $row["birth_year"],
                            $row["death_year"]. "</b>");
                    } else {
                        echo sprintf(
                            $text_format,
                            "../index.php?person&id=". $id,
                            $row["gender"], 
                            $row["first_name"], $row["insertion"], $row["last_name"],
                            $row["birth_year"],
                            $row["death_year"]);
                    }
                }
            }

            if ($spouses) {
                foreach($spouses as $spouse) {
                    echo "<span class='spacer'></span>";
                    $data = $mysqli -> query(sprintf($format_query, $spouse));

                    if ($data->num_rows > 0) {
                        while($row = $data->fetch_assoc()) {
                            echo sprintf(
                                $text_format,
                                "../index.php?person&id=". $spouse,
                                $row["gender"], 
                                $row["first_name"], $row["insertion"], $row["last_name"],
                                $row["birth_year"],
                                $row["death_year"]);
                        }
                    }
                }
            }
            echo "</div>";  
        }
            
        function return_chain($id, $mysqli, $lvls_up, $target = "", $father = TRUE, $lvls_down = 0, $generation = 0) {            
            $children = find_children($id, $mysqli);
            $spouses = find_married($id, $mysqli);
            $generation = $generation + 1;

            echo "<li>";
            
            print_self($id, $spouses, $mysqli, $target);

            if ($children and $generation <= $lvls_up) {
                echo "<ul>";
                foreach ($children as $kid) {
                    
                    return_chain($kid, $mysqli, $lvls_up, $target, $father, $lvls_down, $generation);
                    
                }
                echo "</ul>";

            } 
            echo "</li>";
            return FALSE;
        }
        
        $width = 20000;
        echo "</head><body style='min-width:". $width. ";'>";
        
        if (isset($_GET["no_iframe"])){
            $data = $mysqli -> query(
            "SELECT *
                FROM individuals
                    WHERE id = '". $id. "'"
            );

            // echo $id;
            if ($data->num_rows > 0) {
                // output data of each row
                while($row = $data->fetch_assoc()) {
                    $name_title = $row["first_name"]. " ". $row["insertion"]. " ". $row["last_name"];   
                }
            }
            echo "<title> Stamboom van ". $name_title. "</title></head><body>";
            echo "<div class='pt-5'><b>De Stamboom van ". $name_title. "</b></div>";
        }

        $father = True;
        if (isset($_GET["female"])) {
            $father = FALSE;
        }

        ?>

        <form class="tree" id="FamilyTreeDiv" id="form1">
            <ul>
            <?php
                if (isset($_GET["no_iframe"])){
                    $lvls_down = 500;
                    $lvls_up = 500;
                } 
                
                
                $top = find_top_in_chain($id, $mysqli, $father, $lvls_up);
                return_chain($top, $mysqli, $lvls_down + $lvls_up, $id, $father, $lvls_down, 0);      
            ?>
            </ul>
        <form>
    </body>
</html>
