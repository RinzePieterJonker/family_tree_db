<html lang = "en">
    <head>
        <meta charset="uft-8">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../css/ahnentafel.css" type="text/css">

        <?php 
        include("connect_to_db.php");  
        
        $mysqli = connect_to_db("../config.ini");
        if ($mysqli -> connect_errno) {
            echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
            exit();
        }
        $id = "1";
        if (isset($_GET["id"])){
            $id = $_GET["id"];
        }

        

        if (isset($_GET["no_iframe"])){
            $data = $mysqli -> query(
            "SELECT *
                FROM individuals
                    WHERE id = '". $id. "'"
            );

            // echo $id;
            if ($data->num_rows > 0) {
                // output data of each row
                while($row = $data->fetch_assoc()) {
                    $name_title = $row["first_name"]. " ". $row["insertion"]. " ". $row["last_name"];   
                }
            }
            echo "<title> Kwartierstaat van ". $name_title. "</title></head><body>";
            echo "<div class='pt-5 text-center'><b>De Kwartierstaat van ". $name_title. "</b></div>";
        }
        ?>
        <?php

        function return_parents($child_id, $mysqli) {
            // This function returns the individual_id of the parents for the given child in an array
            $query = 
            "SELECT individual_1_db_id 
                FROM relationships
                    INNER JOIN individuals
                        ON individuals.id = relationships.individual_1_db_id
                            WHERE individual_2_db_id = '". $child_id. "' AND relationship_type='parental'
                                ORDER BY gender DESC";
            $result = $mysqli -> query($query);
            
            $parents = array();
            if ($result->num_rows > 0) {
                while ($row = $result -> fetch_assoc()) {
                    array_push($parents, $row["individual_1_db_id"]);
                }
                return $parents;
            } else {
                return FALSE;
            }
            
        }

        function print_personal_data($row){
            $format = 
            "<li>
                <a target='_parent' href='../index.php?person&id=%s&ahnentafel'> 
                    <span class='text-dark'>
                        <div class='%s'>
                            %s %s %s </br>
                            * %s </br>
                            +%s </br>
                        </div>
                    </span>
                </a>";

            echo sprintf(
                $format,
                $row["id"],
                $row["gender"],
                $row["first_name"], $row["insertion"], $row["last_name"],
                $row["birth_year"],
                $row["death_year"]
            );
        }

        function return_chain($id, $mysqli, $generation, $max = 600){
            // Check hier hoeveel ouders er zijn en of er ouders zijn
            $query = 
            "SELECT *
                FROM individuals
                    WHERE id = '". $id. "'";
            $result = $mysqli -> query($query);
                    
            if ($result->num_rows > 0) {
                while ($row = $result -> fetch_assoc()) {
                    // echo "<li><span>". $row["first_name"]. "</span>";
                    print_personal_data($row);
                }
            }

            $new_generation = $generation + 1;
            $parents = return_parents($id, $mysqli);
        
            if ($parents and $generation <= $max) {
                echo "<ul>";
                foreach($parents as $parent) {
                    return_chain($parent, $mysqli, $new_generation, $max);      
                }
                echo "</ul>";
            }
            echo "</li>";
            return;
        }
        
        if (isset($_GET["lvls"])){
            $n_levels = $_GET["lvls"];
        } else {
            $n_levels = 200;
        }
        
        echo "<div id='developer-tree'><ul>";
        return_chain($id, $mysqli, 1, $n_levels);
        echo "</ul></div>";
        ?>
    </body>
</html>