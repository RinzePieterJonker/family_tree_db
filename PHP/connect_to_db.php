<?php

function connect_to_db ($conf_path) {
    $config_array = parse_ini_file($conf_path);

    $mysqli = new mysqli($config_array["host"], $config_array["username"], $config_array["password"], $config_array["database"]);

    return $mysqli;
}


?>