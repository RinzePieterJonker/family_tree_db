<?php 
    $start = time();
    
    function IsSQLInjection ($input) {
        $injections = array("ALL", "AND", "ANY", "BETWEEN", "EXISTS", "IN", "LIKE", "NOT", "OR", "SOME");
        
        foreach (explode(" ", $input) as $item) {
            $semi_colon = strchr($item, ";");

            if ($semi_colon) {
                return True;
            } 

            $item_upper = strtoupper($item);
            if (in_array($item_upper, $injections)) {
                return True;
            }
        }
        return FALSE;
    }
    // Change this so that it cant be seen in the code, at the moment still on local host
    $mysqli = new mysqli("localhost", "RinzePieter", "7921", "familytreedb");
    $mysqli -> query("drop table if exists results");
    $mysqli -> query(
    "create table Results(
        id                      INT             not null,
        individual_id 			varchar(255) 	not null,
        first_name 				varchar(255),
        insertion 				varchar(100),
        last_name 				varchar(255),
        gender 					varchar(20),
        birth_date 				varchar(255),
        birth_year              varchar(255),
        death_date				varchar(255),
        death_year              varchar(255)
    )");

    if (isset($_POST["advanced"])) {
        $advanced = TRUE;
    } else {
        $advanced = FALSE;
    }

    if(isset($_POST["voornaam"])) {
        $first_name = $_POST["voornaam"];
    } else {
        $first_name = FALSE;
    }

    if(isset($_POST["suffix"])) {
        $suffix = $_POST["suffix"];
    } else {
        $suffix = FALSE;
    }

    if (isset($_POST["achternaam"])) {
        $last_name = $_POST["achternaam"];
    } else {
        $last_name = FALSE;
    }
    $name_array = array($first_name, $suffix, $last_name);
    $order = array("first_name", "insertion", "last_name");

    echo $name_array[0]. " ". $name_array[1]. " ". $name_array[2]. "</br>";

    $all_false = TRUE;
    $injection = FALSE;

    for($i = 0; $i < sizeof($name_array); $i++) {
        $name = $name_array[$i];
        
        if ($name and $name != "") {
            $all_false = FALSE;
            
            if(IsSQLInjection($name)) {
                $injection = TRUE;
                // $format = "Location: ../index.php?search&error&voornaam=%s&suffix=%s&achternaam=%s";
                // header(sprintf($format, $name_array[0], $name_array[1], $name_array[2]));
            } 
        }
    }

    if ($all_false) {
        if ($advanced) {
            header("Location: ../index.php?search&empty&advanced");
        } else {
            header("Location: ../index.php?search&empty");
        }
    } elseif ($injection) {
        if ($advanced) {
            $format = "Location: ../index.php?search&error&advanced&voornaam=%s&suffix=%s&achternaam=%s";
            header(sprintf($format, $name_array[0], $name_array[1], $name_array[2]));
        } else {
            $format = "Location: ../index.php?search&error&voornaam=%s&suffix=%s&achternaam=%s";
            header(sprintf($format, $name_array[0], $name_array[1], $name_array[2]));
        }
        
    } else {
        for($i=0; $i < sizeof($order); $i++){
            $empty_query = "select 1 from results"; 
            $empty = $mysqli -> query($empty_query);
            $select = True;
            if ($empty->fetch_assoc()) {
                $select = FALSE;
            }
                
            // when there is nothing in the table
            if ($name_array[$i]) {
                $names = array();

                if ($select) {
                    $select_everything = "SELECT ". $order[$i]. " FROM individuals";
                } else {
                    $select_everything = "SELECT ". $order[$i]. " FROM results";
                }
                
                
                $all = $mysqli -> query($select_everything);
                
                if ($all->num_rows > 0) {
                    
                    while($row = $all->fetch_assoc()) {
                        $similarity = round(similar_text(strtoupper($row[$order[$i]]), strtoupper($name_array[$i])) / strlen($name_array[$i]) * 100, 2);
                        if ($similarity > 85.00 ) {

                            if (!in_array($row[$order[$i]], $names)) {
                                array_push($names, $row[$order[$i]]);
                                
                            }
                        }
                    }
                }

                $delete_query = "delete from results where";
                $delete = FALSE;

                for($x=0; $x < sizeof($names); $x++) {
                    $name = $names[$x];
                    echo $order[$i]. " - ". $name. "</br>";
                    if ($select) {
                         // order by first_name ASC
                        $query = 
                        "INSERT INTO Results (id, individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, death_date, death_year)
                            SELECT id, individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, death_date, death_year
                                FROM individuals 
                                        WHERE ". $order[$i]. " = '". $name. "'";

                        

                        $mysqli -> query($query);
                    } else {
                        if ($x != 0) {
                            $delete_query = $delete_query. " and";
                        }
                        $delete_query = $delete_query. " not ". $order[$i]. " = '". $name. "'";
                        $delete = TRUE;
                    
                    }
                }
                if ($delete) {
                    $mysqli -> query($delete_query);
                    
                }
            }
        }

    if ($advanced){
        if (isset($_POST["geslacht"])) {
            $gender = $_POST["geslacht"];
            if ($gender != "unknown"){
                $mysqli -> query("DELETE FROM results WHERE NOT gender = '". $gender. "'");
            }
        }
        
        if (isset($_POST["na_geboren"])) {
            $born_after = $_POST["na_geboren"];
            if ($born_after != "") {
                if (IsSQLInjection($born_after)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE birth_year < '". $born_after. "'");
                }
            }

            
        }

        if (isset($_POST["voor_geboren"])) {
            $born_before = $_POST["voor_geboren"];
            if ($born_before != "") {
                if (IsSQLInjection($born_before)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE birth_year > '". $born_before. "'");
                }
            }
        }

        if (isset($_POST["geboren"])) {
            $born = $_POST["geboren"];
            if ($born != "") {
                if (IsSQLInjection($born)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE NOT birth_year = '". $born. "'");
                }
            }
        }

        if (isset($_POST["na_gestorven"])) {
            $died_after = $_POST["na_gestorven"];
            if ($died_after != "") {
                if (IsSQLInjection($died_after)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE NOT death_year < '". $died_after. "'");
                }
            }
        }

        if (isset($_POST["voor_gestorven"])) {
            $died_before = $_POST["voor_gestorven"];
            if ($died_before != "") {
                if (IsSQLInjection($died_before)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE NOT death_year > '". $died_before. "'");
                }
            }
        }

        if (isset($_POST["gestorven"])) {
            $died = $_POST["gestorven"];
            if ($died != "") {
                if (IsSQLInjection($died)) {
                    $format = "Location: ../index.php?search&error&advanced";
                    header($format);
                } else {
                    $mysqli -> query("DELETE FROM results WHERE NOT death_year = '". $died. "'");
                }
            }
        }
        $stop = time() - start;
        header("Location: ../index.php?search&results&advanced");
    } else {
        $stop = time() - start;
        header("Location: ../index.php?search&results&tim");
    }
    }
    $stop

   
?>