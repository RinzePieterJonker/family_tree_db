<html lang = "en">
    <head>
        <meta charset="uft-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta author="Rinze-Pieter Jonker">
        <meta keywords="van der Schaaf, Family tree, stamboom, Rinze-Pieter Jonker, Rinze-Pieter">

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
        
        <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="css/CarouselSlider.css" type="text/css">
        <link rel="stylesheet" href="css/overwrite.css" type="text/css">

        <link rel="icon" type="image/jpg" href="Resources\Images\boom2.jpg">

        <title>Jonker - van der Schaaf stamboom </title>
    </head>

    <body>
        <?php
            include("php/connect_to_db.php");

            function find_id($mysqli) {
                // This function will return the id that will be used for the webpage
                $id = 1;

                // Checking if a id is given via get commands
                if (isset($_GET["id"])){
                    // Setting the id given via get command
                    $id = $_GET["id"];
                } else {
                    // Finding the primary person (Currently Dieuwke Jonker-van der Schaaf
                    $result = $mysqli -> query( "SELECT * FROM individuals WHERE individual_id = '@R1@'");
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $id = $row["id"];
                        }
                    }
                }

                return $id;
            }

            function create_string_arrays($row) {
                // This function is used to create the pre prepared strings for the webpage
                $strings = array();

                $strings["name"] = $row["first_name"]. " ". $row["insertion"]. " ". $row["last_name"];
                

                $birth_date = $row["birth_date"];
                $death_date = $row["death_date"];
                $funeral = $row["crem_burried"];

                // Retrieving the data of the birth and putting it in a string
                if (!empty($birth_date)){
                    if (empty($row["birth_place"])) {
                        $strings["birth_text"] = "Geboren op ". $row["birth_date"];
                    } else {
                        $strings["birth_text"] = "Geboren op ". $row["birth_date"]. " te ". $row["birth_place"];
                    }
                }
               // Retrieving the data of the death and putting it in a string
                if (!empty($death_date)){
                    if (empty($row["death_place"])) {
                        $strings["death_text"] = "Gestorven op ". $row["death_date"];
                    } else {
                        $strings["death_text"] = "Gestorven op ". $row["death_date"]. " te ". $row["death_place"];
                    }
                }
                
                // Creating text for the dates of the cremation or funeral
                if (!empty($funeral)) {
                    $funeral_date = $row["crem_burried_date"];
                    $funeral_place = $row["crem_burried_place"];

                    if ($funeral == "burried") {
                        if (!empty($funeral_date) and !empty($funeral_place)) {
                            $strings["funeral_text"] = "Begraven op ". $funeral_date. " te ". $funeral_place;
                        } elseif (empty($funeral_date) and !empty($funeral_place)) {
                            $strings["funeral_text"] = "Begraven te ". $funeral_place;
                        } elseif (!empty($funeral_date) and empty($funeral_place))
                            $strings["funeral_text"] = "Begraven op ". $funeral_date;
                    
                    // Setting good text if the person was cremated
                    } elseif ($funeral == "cremated") {
                        if (!empty($funeral_date) and !empty($funeral_place)) {
                            $strings["funeral_text"] = "Gecremeerd op ". $funeral_date. " te ". $funeral_place;
                        } elseif (empty($funeral_date) and !empty($funeral_place)) {
                            $strings["funeral_text"] = "Gecremeerd te ". $funeral_place;
                        } elseif (!empty($funeral_date) and empty($funeral_place))
                            $strings["funeral_text"] = "Gecremeerd op ". $funeral_date;
                    }
                }

                return $strings;

            }

            function find_relatives($id, $mysqli) {
                // Creating a query to retrieve the parents
        
                $parents_query = 
                "SELECT *
                    FROM relationships
                        INNER JOIN individuals
                            ON individuals.id = relationships.individual_1_db_id
                                WHERE individual_2_db_id = '". $id. "' AND relationship_type = 'parental'";
    
                // Creating the query to find the spouse(s)
                $married_query = 
                "SELECT *
                    FROM relationships
                        INNER JOIN individuals 
                            ON individuals.id = relationships.individual_2_db_id
                                WHERE individual_1_db_id = '". $id. "' AND relationship_type = 'marriage'";

                // Creating a query to retrieve the children
                $children_query = 
                "SELECT *
                    FROM relationships
                        INNER JOIN individuals 
                            ON individuals.id = relationships.individual_2_db_id
                                WHERE individual_1_db_id = '". $id. "' and relationship_type = 'parental'";

               
                // Saving the results
                $results = array(
                    "parents" => $mysqli -> query($parents_query),
                    "spouse" => $mysqli -> query($married_query),
                    "children" => $mysqli -> query($children_query)
                );

                // Handeling the results
                foreach($results as $item_key => $item_value) {
                    if ($item_value->num_rows > 0) {
                        $data = array();
                        while($row = $item_value->fetch_assoc()) {
                            array_push($data, array(
                                $row["id"],
                                $row["first_name"]. " ". $row["insertion"]. " ". $row["last_name"],
                                $row["relationship_date"],
                                $row["relationship_place"]
                            ));

                        }
                        $results[$item_key] = $data;
                    } else {
                        unset($results[$item_key]);
                    }

                }

                if (array_key_exists("parents", $results)) {
                    $siblings = array();
                    
                    // for adding the siblings from each parent, this also includes step siblings
                    foreach($results["parents"] as $parent) {
                        // Creating a query to retrieve the siblings
                        $sibling_query = 
                        "SELECT *
                            FROM relationships 
                                INNER JOIN individuals 
                                    ON individuals.id = relationships.individual_2_db_id
                                        WHERE individual_1_db_id = '". $parent[0]. "' and relationship_type='parental' and individual_2_db_id != '". $id. "'
                                            ORDER BY birth_year";
                        $result = $mysqli -> query($sibling_query);
                        
                        // parsing the results from the query and adding them to the siblings array if there is no duplicate, this also adds the step-siblings
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $data = array(
                                    $row["id"],
                                    $row["first_name"]. " ". $row["insertion"]. " ". $row["last_name"],
                                    $row["relationship_date"],
                                    $row["relationship_place"]
                                );
                                if (!in_array($data, $siblings)) {
                                    array_push($siblings, $data);
                                }

                            }
                        }
                     $results["siblings"] = $siblings;   
                    }
                }

                return $results;
            }

            function find_personal_info ($id, $mysqli, $image_location = "Resources/Images/") {
                // This function is used for getting all the personal info of one specific person
                // If all the information is found it will return all the needed info in a array
                $info = array();
                $result =  $mysqli -> query("SELECT * FROM Individuals WHERE id='". $id. "'");

                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        foreach($row as $item_key => $item_value) {
                            
                            // Checking if the multi element items are also given, since these need to be extracted
                            if (in_array($item_key, array("img_path", "events", "residences", "occupations"))) {
                                $item_value = explode(" | ", $item_value);
                                
                                // Adding the file directory to the 
                                if ($item_key == "img_path" and $item_value[0] != "") {
                                    for($i = 0; $i < sizeof($item_value); $i++ ) {
                                        $item_value[$i] = $image_location. $item_value[$i];
                                    }  
                                }
                                
                            }

                            // Adding the value to the info array                            
                            $info[$item_key] = $item_value;


                        }
                        $info = array_merge($info, create_string_arrays($row));
                    }
                }
                $info = array_merge($info, find_relatives($id, $mysqli));

                return $info;

            }
            
            $mysqli = connect_to_db("config.ini");

            if ($mysqli -> connect_errno) {
                echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
                exit();
            }
            
            $id = find_id($mysqli);
            $info = find_personal_info($id, $mysqli);
            
        ?>

        <!-- creating the navbar -->
        <nav class="navbar navbar-expand navbar-dark bg-dark">
            <a class="navbar-brand font-weight-bold px-5" href="index.php">
                <!-- <img src="resources\images\icon.jpg" width="30" height="30" class="d-inline-block align-top rounded" alt=""> -->
                Jonker - van der Schaaf stamboom
            </a>
            <div class="navbar-collapse collapse">
                <ul class="navbar-nav ml-auto pt-1">
                    <li class="nav-item active px-3">
                        <a class="nav-link" href="index.php"> Home <span class="sr-only">(current) </span></a>
                    </li>
                    <li class="nav-item active px-3">
                        <a class="nav-link" href="index.php?person"> Personen <span class="sr-only">(current) </span></a>
                    </li>
                    <li class="nav-item active px-3">
                        <a class="nav-link" href="index.php?search"> Zoeken <span class="sr-only">(current) </span></a>
                    </li>
                </ul>
            </div>
        </nav>

       
        <?php if (isset($_GET["search"])) { ?>

            <div class="row mx-5 p-3">
                <div class="col container bg-white rounded p-3"> 
                    <h2 class="text-center"> Persoon vinden </h2> 
                        <form id="search_form" action="php/search.php" method="post">
                        <div class="form-row my-2">
                            <div class="form-group col-md-5">
                                <label for="voornaam"> Voornaam </label>
                                <input type="voornaam" name="voornaam" class="form-control" id="voornaam" placeholder="Voornaam">
                            </div>

                            <div class="form-group col-md-2">
                                <label for="suffix"> Tussenvoegsel </label>
                                <input name="suffix" type="suffix" class="form-control" id="suffix" placeholder="Tussenvoegsel">
                            </div>

                            <div class="form-group col-md-5">
                                <label for="achternaam"> Achternaam </label>
                                <input name="achternaam" type="achternaam" class="form-control" id="achternaam" placeholder="Achternaam">
                            </div>
                        </div>

                        <?php if (isset($_GET["advanced"])) { ?>
                        
                        <div class="form-row">
                            <div class="form-group col-md-5">
                                <label for="geslacht"> Geslacht </label>
                                <select name="geslacht" id="geslacht" class="form-control">
                                    <?php 
                                    $genders = $mysqli -> query("SELECT DISTINCT gender FROM individuals");
                                    if ($genders->num_rows > 0){
                                        while($row = $genders->fetch_assoc()) {
                                            $nl_gender = "onbekend";

                                            if ($row["gender"] == "male") {
                                                $nl_gender = "man";
                                            } elseif ($row["gender"] == "female") {
                                                $nl_gender = "vrouw";
                                            }
                                            
                                            echo "<option value='". $row["gender"]. "'>". $nl_gender. "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="na_geboren"> Na jaartal geboren </label>
                                <input type="na_geboren" name="na_geboren" class="form-control" id="na+geboren" placeholder="Jaartal">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="voor_geboren"> Voor jaartal geboren </label>
                                <input type="voor_geboren" name="voor_geboren" class="form-control" id="voor_geboren" placeholder="Jaartal">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="geboren"> In jaartal geboren </label>
                                <input type="geboren" name="geboren" class="form-control" id="geboren" placeholder="Jaartal">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="na_gestorven"> Na jaartal gestorven </label>
                                <input type="na_gestorven" name="na_gestorven" class="form-control" id="na_gestorven" placeholder="Jaartal">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="voor_gestorven"> Voor jaartal gestorven </label>
                                <input type="voor_gestorven" name="voor_gestorven" class="form-control" id="voor_gestorven" placeholder="Jaartal">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="gestorven"> In jaartal gestorven </label>
                                <input type="gestorven" name="gestorven" class="form-control" id="gestorven" placeholder="Jaartal">
                            </div>
                        </div>

                        <div class="form-row my-2">
                            <a href="?search" class="btn btn-outline-dark"> Normaal zoeken </a>
                        </div>

                        <input type="hidden" id="advanced" name="advanced" value="TRUE">

                        <?php } else {?>
                        
                        <div class="form-row my-2">
                            <a href="?search&advanced" class="btn btn-outline-dark"> Geavanceerd zoeken </a>
                        </div>
                        
                        <?php }?>
                    
                        <div class="form-row my-2"> 
                            <div class="form-group"> 
                                <input class="btn btn-outline-dark" type="submit" name="zoeken" value="Zoeken">
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <?php 
            // $mysqli -> query("DROP IF EXISTS results");
            $results_exists = "";                   
            if (isset($_GET["error"])) { 
            
            ?>
                <div class="row mx-5 my-3 rounded alert alert-danger" role="alert">
                    <div class="col countainer rounded p-3">
                        Error gevonden in zoekopdracht; </br>
                        <?php echo "Voornaam: ". $_GET["voornaam"]. "</br> Tussenvoegsel: ". $_GET["suffix"]."</br> Achternaam: ". $_GET["achternaam"];?>
                    </div>
                </div>
            
            
            <?php } elseif(isset($_GET["results"])) { ?>
                
                <div class="row mx-5 my-3">
                    <div class="col countainer rounded bg-white">
                        <h2 class="text-center">Resultaten</h2></br>
                        
                        <?php 
                        $search_query = "SELECT * FROM Results ORDER BY birth_year DESC, first_name ASC, insertion ASC, last_name ASC";
                        $search_results = $mysqli -> query($search_query);

                        echo $search_results->num_rows. " resultaten gevonden";

                        if ($search_results->num_rows > 0){
                            while($row = $search_results->fetch_assoc()) {
                                $gender = "Man";
                                if ($row["gender"] != "male") {
                                    $gender = "Vrouw";
                                }
                                
                                $format = 
                                "<div class='col rounded text-dark p-3 my-2  bg-light'>
                                    <a href='?id=%s&person' class='nav-link text-dark'>
                                    %s %s %s </br>
                                    %s </br>
                                    %s - %s </a></div>";
                                echo sprintf($format, $row["id"], $row["first_name"], $row["insertion"], $row["last_name"], $gender, $row["birth_year"], $row["death_year"]);
                            }
                        }
                        ?>
                    </div>
                </div>
            
            <?php } elseif(isset($_GET["empty"])) {?>
                <div class="row mx-5 my-3 rounded alert alert-danger" role="alert">
                    <div class="col countainer rounded p-3">
                        Geen zoekopdracht gekregen
                    </div>
                </div>

            <?php }?>
        <?php } elseif (isset($_GET["person"])) { ?>
    	    <div class="row mx-5 py-3 trees">
                <div class="row mx-4">
                    <?php 
                    if (isset($_GET["ahnentafel"])) {
                        echo 
                        "<a href='index.php?person&id=". $id. "' class='label inactive'><div class='col-6 col-sm-2'><span>Stamboom</span></div></a>
                        <a href='index.php?person&id=". $id. "&ahnentafel' class='label ml-1 active'><div class='col-6 col-sm-2 label'><span>Kwartierstaat</span></div></a>";
                    } else {
                        echo 
                        "<a href='index.php?person&id=". $id. "' class='label active'><div class='col-6 col-sm-2'><span>Stamboom</span></div></a>
                        <a href='index.php?person&id=". $id. "&ahnentafel' class='label inactive ml-1'><div class='col-6 col-sm-2 label'><span>Kwartierstaat</span></div></a>";
                    }
                    ?>
                    
                </div>
                <div class="row">
                    <div class="col rounded bg-white">
                        <?php
                        
                        if (isset($_GET["ahnentafel"])) {
                            echo "<p class='text-center'><b>De Kwartierstaat van ". $info["name"]. " 4 generaties diep</b></p>";
                            echo "<p class='text-center'><a href='php/ahnentafel.php?id=". $id. "&no_iframe' target='_blank'>link naar de voledige kwartierstaat</a></p>";
                            echo "<iframe src='php/ahnentafel.php?id=". $id. "&lvls=4' style='border: none;height: 660px;width: 100%; cursor: grab; overflow:'></iframe>"; 
                        } else {
                            echo  "<p class='text-center'><b>De stamboom van ". $info["name"]. "</b></p>";
                            echo "<p class='text-center'><a href='php/family_tree.php?id=". $id. "&no_iframe' target='_blank'>link naar de voledige Stamboom</a></p>";
                            echo "<iframe src='php/family_tree.php?id=". $id. "' style='border: none;height: 330px;width: 100%; cursor: grab; overflow:'></iframe>"; 
                        }

                        ?>    
                    </div>
                </div>
            </div>
            
            <div class="row mx-3" id="info">
                <div class="col container rounded bg-white mx-5 mx-3 text-justify">
                <h1 class="font-weight-bold text-center"> <?php echo $info["name"];?> </h1>
                    <div class="row">
                        <div class="col-7 text-justify" id="information text">
                            <?php 
                            if (array_key_exists("birth_text", $info)) {
                                echo $info["birth_text"]. ".</br>";
                            }

                            if (array_key_exists("death_text", $info)) {
                                echo $info["death_text"]. ".</br>";
                            }

                            if (array_key_exists("funeral_text", $info)) {
                                echo $info["funeral_text"]. ".</br>";
                            }
                            ?>
                            
                            </br>
                            <?php 
                            if ($info["residences"][0] != "") {
                                echo "<b> Adressen en woningen</b><ul class='px-3'>";

                                foreach($info["residences"] as $resi) {
                                    echo "<li>". $resi. ".</li>";
                                }
                                echo "</ul>";

                            }

                            if ($info["occupations"][0] != "") {
                                echo "<b> Banen </b><ul class='px-3'>";

                                foreach($info["occupations"] as $occu) {
                                    echo "<li>". $occu. ".</li>";
                                }
                                
                                echo "</ul>";
                            }
                            
                            if ($info["events"][0] != "") {
                                echo "<b> Gebeurtenissen </b><ul class='px-3'>";

                                foreach($info["events"] as $even) {
                                    echo "<li>". $even. ".</li>";
                                }
                                
                                echo "</ul>";
                            }
                            ?>
                            </br>
                            <h2><b> Relaties en familie </b></h2>
                            <?php 

                            // Setting the text for the spouses
                            if (array_key_exists("spouse", $info)){
                                foreach($info["spouse"] as $spouse) {
                                    echo "Getrouwd met ". $spouse[1]; 
                                    if ($spouse[2] != "") {
                                        echo " op ". $spouse[2];
                                    }
                                }
                                echo "</br>";
                            }
                            
                            // Setting the text for the parents
                            if (array_key_exists("parents", $info)) {
                                echo "<br/><b> Ouders </b><ul class='px-3'>";
                                foreach($info["parents"] as $parent) {
                                    echo "<li>". $parent[1]. ".</li>";
                                }
                                echo "</ul>";
                            }
                            
                            if (array_key_exists("siblings", $info)) {
                                echo "<br/><b> Broers en zussen </b><ul class='px-3'>";
                                foreach($info["siblings"] as $sibling) {
                                    echo "<li>". $sibling[1]. ".</li>";
                                }
                                echo "</ul>";
                            }

                            if (array_key_exists("children", $info)) {
                                echo "<br/><b> Kinderen </b><ul class='px-3'>";
                                foreach($info["children"] as $kid) {
                                    echo "<li>". $kid[1]. ".</li>";
                                }
                                echo "</ul>";
                            }

                           
                            ?>
                        </div>
                        <div class="col-5"> 
                            <?php 
                            // Creating the carousel slider for the images since the carousel slider from bootstrap didnt work
                            
                            // Checking if there are any pictures
                            if (!empty($info["img_path"])) {
                                if (sizeof($info["img_path"]) == 1) {
                                    // Just ignoring the carousel if there is just one image
                                    echo "<img class='rounded img-fluid' src='". $info["img_path"][0]. "'/>";
                                } else {

                                    // Creating the dynamic carousel if there is more than 1 image
                                    // Works mostly with css
                                    echo "<div class='carousel'>";
                                    
                                    for ($i = 0; $i < sizeof($info["img_path"]); $i++) {
                                        if ($i == 0) {
                                            echo "<input id='image1'type='radio' name='image-selector'checked='checked' />";
                                            echo "<label for='image". sizeof($info["img_path"]). "'> View image 1 </label>";
                                            echo "<img src='". $info["img_path"][$i]. "' class='rounded' />";
                                        } else {
                                            echo "<input id='image". ($i + 1). "' type='radio' name='image-selector' \>";
                                            echo "<label for='image". $i. "'> View image 2 </label>";
                                            echo "<img src='". $info["img_path"][$i]. "' class='rounded' />";
                                        }
                                    }
                                    echo "<label for='image2'>View image 2</label><label for='image1'>View image 1</label></div>";
                                }
                            }
                            ?>
                        </div>

                    <div class="row my-2">
                        <div class="col container rounded bg-white mx-3 text-justify">
                            <?php 

                            function find_notes($id) {

                            }

                            function find_sources($id) {

                            }

                            // optimize this further and try to get these in 2 querys
                            $notes_individuals = "select * from notes where id = '". $id. "'" ;
                            $notes_relations = 
                            "select * 
                                from notes
                                    inner join relationships
                                        on relationships.relationship_id = notes.relationship_id 
                                            where individual_1_db_id = '". $id. "' or individual_2_db_id = '". $id.  "'";

                            $sources_individuals = "select * from sources where id = '". $id. "'" ;
                            $sources_relations = 
                            "select * 
                                from sources
                                    inner join relationships
                                        on relationships.relationship_id = sources.relationship_id 
                                            where individual_1_db_id = '". $id. "' or individual_2_db_id = '". $id. "'";
                            
                            $sources_individuals_results = $mysqli -> query($sources_individuals);
                            $sources_relations_results = $mysqli -> query($sources_relations);
                            $notes_individuals_results = $mysqli -> query($notes_individuals);
                            $notes_relations_results = $mysqli -> query($notes_relations);

                            if ($sources_individuals_results->num_rows > 0 or $sources_relations_results->num_rows > 0 or $notes_individuals_results->num_rows > 0 or $notes_relations_results->num_rows > 0) {
                                echo "<h2><b> Notities en Bronnen </b></h2>";

                                if ($sources_individuals_results->num_rows > 0 or $sources_relations_results->num_rows > 0) {
                                    echo "<b> Bron </b><ul class='px-3'>";
                                    
                                    while($row = $sources_individuals_results -> fetch_assoc()) {
                                        echo "<li>". $row["source"]. "</li>";
                                    }

                                    while($row = $sources_relations_results -> fetch_assoc()) {
                                        echo "<li>". $row["source"]. "</li>";
                                    }
                                    echo "</ul>";
                                }

                                if ($notes_individuals_results->num_rows > 0 or $notes_relations_results->num_rows > 0) {
                                    echo "<b> Notities </b><ul class='px-3'>";
                                    
                                    while($row = $notes_individuals_results -> fetch_assoc()) {
                                        echo "<li>". $row["note"]. "</li>";
                                    }

                                    while($row = $notes_relations_results -> fetch_assoc()) {
                                        echo "<li>". $row["note"]. "</li>";
                                    }
                                    echo "</ul>";
                                }
                            }
                            
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        <?php } else { ?>
            <div class="row bg-white rounded mx-5 p-3"> 
                Hier komt nog informatie over de website over waarom en hoe je het moet gebruiken
            </div>
        <?php }

        //closing the connection
        $mysqli -> close();
        ?>
    
    </body>
</html>
