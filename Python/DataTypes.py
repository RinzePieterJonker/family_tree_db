#!/usr/bin/env python3

import mysql.connector
import re

MONTHS = {
	"JAN": "januari", 1: "januari",
	"FEB": "februari", 2 : "februari",
	"MAR": "maart", 3 : "maart",
	"APR": "april", 4 : "april",
	"MAY": "mei", 5 : "mei",
	"JUN": "juni", 6: "juni",
	"JUL": "juli", 7 : "juli",
	"AUG": "augustus", 8 : "augustus",
	"SEP": "september", 9 : "september",
	"OCT": "oktober", 10 : "oktober",
	"NOV": "november", 11 : "november",
	"DEC": "december", 12 : "december",

	"BEF": "voor",
	"AFT": "na",

	"FROM": "van",
	"TO": "tot"
}

TAGS = {
	"EVEN": "gebeurtenis",
	"RESI": "woning",
	"OCCU": "baan"
}

# CODE
def fix_dates(date, delim = "-"):
	new_date = []

	date_list = date.split(delim)
	for item in date_list:
		try:
			new_date.append(MONTHS[item])
		except KeyError:
			new_date.append(item)

	return " ".join(new_date)


def auto_escape(string: str):
	to_escape = ["'"]
	correction = ["_c_"]
	for c in range(len(to_escape)):
		string = string.replace(to_escape[c], correction[c])
	return string


def remove_duplicates(lst: list):
	"""
	This function removes the duplicates from any list and then returns the list without any duplicates
	:param lst:
	:return:
	"""
	new_lst = []
	for item in lst:
		if item not in new_lst:
			new_lst.append(item)

	return new_lst

class Person:
	# Still need to add EVEN and Residence
	def __init__(self, aldfear_id):
		self.aldfear_id = aldfear_id

		self.data = {
			"individual_id": aldfear_id,
			"first_name": None,
			"insertion": None,
			"last_name": None,
			"gender": "unknown",
			"occupations": None,
			"residences": None,
			"birth_date": None,
			"birth_year": None,
			"birth_place": None,
			"death_date": None,
			"death_year": None,
			"death_place": None,
			"crem_burried": None,
			"crem_burried_date": None,
			"crem_burried_place": None,
			"birth_fam_id": None,
			"married_fam_id": None,
			"img_path" : None,
			"events": None
		}

		self.current_type = None
		self.previous_type = None

		self.occupations = []
		self.images = []
		self.residences = []
		self.events = []

		self.notes = []
		self.sources = []

	def add_data(self, line):
		# Eventually add pictures to the database and this function

		if self.current_type == "RESI":
			if line[0] != "1" and line[2] != "" and line[1] != "SOUR" and line[1] != "NOTE":
				self.residences[-1].append(" ".join(line[2:]))
		if self.current_type == "OCCU":
			if line[0] != "1" and line[2] != "" and line[1] != "SOUR" and line[1] != "NOTE":
				self.occupations[-1].append(" ".join(line[2:]))
		if self.current_type == "EVEN":
			if line[0] != "1" and line[1] != "_OBJE" and line[1] != "SOUR" and line[1] != "NOTE" and line[2] != "" :
				self.events[-1].append(" ".join(line[2:]))


		if line[1] == "FILE":
			if line[2] not in self.images:
				self.images.append(" ".join(line[2:]))

		if line[0] == "1":
			# Checking for the current type and reacting to it
			self.previous_type = self.current_type
			self.current_type = line[1]
			if self.current_type == "RESI":
				self.residences.append([])
			elif self.current_type == "OCCU":
				self.occupations.append([" ".join(line[2:])])
			elif self.current_type == "EVEN":
				self.events.append([])


			if not len(line) <= 2 and line[2] != "":
				if self.current_type == "NAME":
					# handeling the first and last name

					split_name = " ".join(line[2:]).split("/")
					self.data["first_name"] = split_name[0]

					# Parsing the last name and removing the suffix if i is still on there
					last_name = split_name[-2]
					if not last_name == "" and not last_name[0].isupper():
						last_name = last_name.split(" ")[-1]

					self.data["last_name"] = last_name

				elif self.current_type == "SEX":
					# Adding gender
					self.data["gender"] = "female" if line[2] == "F" else "male"

				elif self.current_type == "FAMC":
					# Adding birth family id
					self.data["birth_fam_id"] = line[2]

				elif self.current_type == "FAMS":
					# Adding married family id
					self.data["married_fam_id"] = line[2]

		elif line[0] == "2":
			# handeling the sup variables for some of the variables like NAME with SPFX
			if self.current_type == "NAME" and line[1] == "SPFX":
				# Handling the suffix
				self.data["insertion"] = " ".join(line[2:])

			elif self.current_type == "BIRT":
				# Adding birt place and dates
				if line[1] == "DATE":
					self.data["birth_date"] = fix_dates("-".join(line[2:]))
					self.data["birth_year"] = line[-1]
				elif line[1] == "PLAC":
					self.data["birth_place"] = " ".join(line[2:])

			elif self.current_type == "DEAT":
				# Adding death place and dates
				if line[1] == "DATE":
					self.data["death_date"] = fix_dates("-".join(line[2:]))
					self.data["death_year"] = line[-1]
				elif line[1] == "PLAC":
					self.data["death_place"] = " ".join(line[2:])

			elif self.current_type == "BURI" or self.current_type == "CREM":
				self.data["crem_burried"] = "cremated" if self.current_type == "CREM" else "burried"
				if line[1] == "DATE":
					self.data["crem_burried_date"] = fix_dates("-".join(line[2:]))
				elif line[1] == "PLAC":
					self.data["crem_burried_place"] = " ".join(line[2:])

			elif line[1] == "NOTE":
				self.notes.append([line[2], self.current_type])

			elif line[1] == "SOUR":
				self.sources.append([line[2], self.current_type])

			elif self.current_type == "EVEN":
				if line[1] == "DATE":
					pass
				elif line[1] == "TYPE":
					pass

	def insert_data(self, cursor, cnx):
		# combining all the events into one string with the correct dates and delims
		for c, item in enumerate(self.events):
			item[-1] = fix_dates(item[-1], delim = " ")
			self.events[c] = " ".join(item)
		self.data["events"] = " | ".join(self.events)

		# Combining all the occupations into one string with the correct dates and delims
		for c, item in enumerate(self.occupations):
			item[-1] = fix_dates(item[-1], delim = " ")
			self.occupations[c] = " ".join(item)
		self.data["occupations"] = " | ".join(self.occupations)

		# Combining all the residences into one string with the correct dates and delims
		if len(self.residences) != 0 and len(self.residences[0]) > 0:
			for c, item in enumerate(self.residences):
				item[-1] = fix_dates(item[-1], delim=" ")
				self.residences[c] = " ".join(item)
			self.data["residences"] = " | ".join(self.residences)

		if len(self.images) != 1:
			self.images = remove_duplicates(self.images)
			self.data["img_path"] = " | ".join(self.images[::-1])
		else:
			self.data["img_path"] = self.images[0]

		# cursor.execute("select * from individuals where individual_id='{}'".format(self.aldfear_id))

		not_in_db = True
		# if cursor.fetchall():
		# 	not_in_db = False
		# 	if True: # self.replace:
		# 		self.remove_from_db(cursor, cnx)

		if not_in_db:
			keys = "("
			for c, key in enumerate(self.data.keys()):
				if c != 0:
					keys += ", {}".format(key)
				else:
					keys += key

			values = tuple([x if x else "" for x in self.data.values()])

			insert_query = "insert into Individuals{}) values{}".format(keys, values)

			cursor.execute(insert_query)

			cnx.commit()

			self.add_notes_and_sources(cursor, cnx)

			if self.data["insertion"] is None:
				print(" > Added individual {} {} with id {} to the database".format(self.data["first_name"],
																					   self.data["last_name"],
																					   self.data["individual_id"]))
			else:
				print(" > Added individual {} {} {} with id {} to the database".format(self.data["first_name"],
																					   self.data["insertion"],
																					   self.data["last_name"],
																					   self.data["individual_id"]))

	def remove_from_db(self, cursor, cnx):

		# Misschien handig om pas toe te voegen wanneer alle data binnen is

		# Kijken of deze persoon ook in relations zit
		#	als deze er in zitten verwijderen uit deze relations dan uit families, sources en notes
		# verwijderen uit relations
		# verwijdern uit sources en notes voor de individu
		# verwijderen uit individuals

		pass

	def find_id(self, cursor, cnx):
		cursor.execute("SELECT MAX(id) FROM individuals")
		return cursor.fetchall()[0][0]

	def add_notes_and_sources(self, cursor, cnx):
		# Adding all the notes for the individual without the text

		id = self.find_id(cursor, cnx)

		for note in self.notes:
			# changing the about to something that can be added to the website
			try:
				note[1] = "bij " + TAGS[note[1]]
			except KeyError:
				note[1] = "bij " + note[1]

			insert_note = "insert into Notes(individual_id, note_id, about) values({}, '{}', '{}')".format(
				id,
				note[0],
				note[1]
			)
			cursor.execute(insert_note)

			cnx.commit()

		# Adding all the sources for the invididual without the text
		for source in self.sources:
			# Fixing the sources so that they kan be added neatly to the website
			try:
				source[1] = "bij " + TAGS[source[1]]
			except KeyError:
				source[1] = "bij " + source[1]

			insert_note = "insert into Sources(individual_id, source_id, source_type) values('{}', '{}', '{}')".format(
				id,
				source[0],
				source[1]
			)

			cursor.execute(insert_note)

			cnx.commit()


class Family:
	def __init__(self, aldfear_id):
		self.aldfear_id = aldfear_id

		self.data = {
			"family_id": aldfear_id,
			"head_of_family": "",
			"husband": "",
			"wife": ""
		}

	def add_data(self, line):
		if line[0] == "1":
			if line[1] == "HUSB":
				self.data["husband"] = line[2]
				self.data["head_of_family"] = line[2]
			elif line[1] == "WIFE":
				self.data["wife"] = line[2]
				if self.data["head_of_family"] == "":
					self.data["head_of_family"] = line[2]


	def insert_data(self, cursor, cnx):
		# find insertion and last name and add them
		try:
			cursor.execute("select insertion, last_name from individuals where individual_id = '{}'".format(self.data["husband"]))
			results = cursor.fetchall()

			insertion = ""
			last_name = ""
			if results:
				insertion, last_name = results[0]


			insert_query = "insert into Families(family_id, head_of_family,  husband, wife, insertion, last_name) values('{}', '{}', '{}', '{}', '{}', '{}')".format(
				self.data["family_id"],
				self.data["head_of_family"],
				self.data["husband"],
				self.data["wife"],
				insertion,
				last_name
			)

			cursor.execute(insert_query)
			cnx.commit()

			print(" > Added family {} with id {} to the database".format(last_name, self.data["family_id"]))
		except mysql.connector.errors.IntegrityError as err:
			print(" > Family with id = {} already in the database, skipping this family".format(self.data["family_id"]))
			pass


class RelationShips:
	def __init__(self, aldfear_id):
		self.aldfear_id = aldfear_id

		self.current_type = None

		self.id_1 = "Null"
		self.gender_head = ""
		self.parents = [None, None]

		self.relations = []
		self.notes = []
		self.sources = []

	def add_data(self, line):

		if line[0] == "1":
			if line[1] == "HUSB":
				self.id_1 = line[2]
				self.gender_head = "M"
				self.parents[0] = line[2]
			elif line[1] == "WIFE":
				self.parents[1] = line[2]
				if self.id_1 == "Null":
					self.id_1 = line[2]
					self.gender_head = "F"
				else:
					self.relations.append([self.aldfear_id, self.id_1, line[2], "husband", "wife", "marriage", "Null", "Null"])
			elif line[1] == "CHIL":
				for count, parent in enumerate(self.parents):
					if parent:
						self.relations.append([self.aldfear_id,
											   parent,
											   line[2],
											   "father" if count == 0 else "mother",
											   "child"])


			elif line[1] == "MAR":
				self.current_type = "MAR"

		elif line[0] == "2":
			if line[1] == "NOTE":
				self.notes.append([line[2], "MAR", self.parents])

			elif line[1] == "SOUR":
				self.sources.append([line[2], "MAR", self.parents])

			elif line[1] == "TYPE":
				self.relations[-1][5] = "marriage"

			elif line[1] == "DATE":
				self.relations[-1][6] = fix_dates(" ".join(line[2:]), delim=" ")

			elif line[1] == "PLAC":
				self.relations[-1][7] = " ".join(line[2:])

	def insert_data(self, cursor, cnx):
		id_query = "select MAX(id) from individuals where individual_id = '{}'"

		for relation in self.relations:
			queries = [id_query, id_query]

			c = 1
			db_ids = []

			for item in queries:
				cursor.execute(item.format(relation[c], relation[0]))
				result = cursor.fetchall()
				db_ids.append(result[0][0])
				c += 1

			if relation[4] == "child":
				insert_statement = ["insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_role, individual_2_role, individual_1_db_id, individual_2_db_id, relationship_type) values('{}', {}, {}, 'parental')".format("', '".join(relation), db_ids[0], db_ids[1])]
			else:
				insert_statement = [
					"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_role, individual_2_role, relationship_type, relationship_date, relationship_place, individual_1_db_id, individual_2_db_id) values('{}', {}, {})".format(
						"', '".join(relation), db_ids[0], db_ids[1]),
					"insert into Relationships(family_id, individual_2_id, individual_1_id, individual_2_role, individual_1_role, relationship_type, relationship_date, relationship_place, individual_1_db_id, individual_2_db_id) values('{}', {}, {})".format(
						"', '".join(relation), db_ids[1], db_ids[0])
					]

			select_query = "select * from relationships where individual_1_id = '{}' and individual_2_id = '{}' and family_id = '{}'".format(
				relation[1],
				relation[2],
				relation[0]
			)
			cursor.execute(select_query)
			if not cursor.fetchall():
				for query in insert_statement:
					cursor.execute(query)

				cnx.commit()
				print(" > Added relation in family {} between {} and {} to the relation table".format(relation[0], relation[1], relation[2]))

			self.add_notes_and_sources(cursor, cnx)

	def add_notes_and_sources(self, cursor, cnx):
		# retrieve relationship_id
		for note in self.notes:
			select_query = "select relationship_id from relationships where individual_1_id = '{}' and individual_2_id = '{}'".format(
				note[2][0],
				note[2][1]
			)
			cursor.execute(select_query)
			rel_id = cursor.fetchall()[0][0]

			insert_query = "insert into Notes(relationship_id, note_id, about) values('{}', '{}', '{}')".format(
				rel_id,
				note[0],
				note[1]
			)
			# checking if note already in notes table
			check_query = "select * from Notes where relationship_id = '{}' and note_id = '{}'".format(rel_id, note[0])
			cursor.execute(check_query)
			if not cursor.fetchall():
				cursor.execute(insert_query)
				cnx.commit()
				print(" > Added note for relationship with id {} to Notes table".format(note[0]))
			else:
				print(" > Skipped Note because it was already in database")


		for source in self.sources:
			select_query = "select relationship_id from relationships where individual_1_id = '{}' and individual_2_id = '{}'".format(
				source[2][0],
				source[2][1]
			)
			cursor.execute(select_query)
			rel_id = cursor.fetchall()[0][0]

			insert_query = "insert into Sources(relationship_id, source_id, source_type) values('{}', '{}', '{}')".format(
				rel_id,
				source[0],
				source[1]
			)
			# checking if note already in notes table
			check_query = "select * from Sources where relationship_id = '{}' and source_id = '{}'".format(rel_id, source[0])
			cursor.execute(check_query)
			if not cursor.fetchall():
				cursor.execute(insert_query)
				cnx.commit()
				print(" > Added note for relationship with id {} to Sources table".format(source[0]))
			else:
				print(" > Skipped Note because it was already in database")

		pass


class Notes:
	def __init__(self, aldfear_id):
		self.aldfear_id = aldfear_id

		self.note = ""

	def add_data(self, line):
		if line[0] == "1":
			if line[1] == "CONC":
				self.note = " ".join(line[2:])
		pass

	def insert_data(self, cursor, cnx):
		id = self.find_id(cursor)
		if id :
			update_query = "update Notes set note = '{}' where note_id = '{}' and id = {}".format(
				auto_escape(self.note),
				self.aldfear_id,
				id)
			update_query = update_query

			cursor.execute(update_query)
			cnx.commit()

			print(" > Updated note with id {}".format(self.aldfear_id))
		else:
			print(" > skipped note {} since it was not mentioned in the database".format(self.aldfear_id))

	def find_id(self, cursor):
		cursor.execute("select MAX(id) from notes where note_id = '{}'".format(self.aldfear_id))
		results = cursor.fetchall()
		try:
			return results[0][0]
		except IndexError:
			return None


class Sources:
	def __init__(self, aldfear_id):
		self.aldfear_id = aldfear_id

		self.source = ""

	def add_data(self, line):
		if line[0] == "2":
			if line[1] == "CONC":
				self.source = " ".join(line[2:])

	def find_id(self, cursor):
		cursor.execute("select MAX(id) from sources where source_id = '{}'".format(self.aldfear_id))
		results = cursor.fetchall()
		try:
			return results[0][0]
		except IndexError:
			return None

	def insert_data(self, cursor, cnx):
		id = self.find_id(cursor)
		if id:
			update_query = "update Sources set source = '{}' where source_id = '{}' and id = {}".format(
				auto_escape(self.source), self.aldfear_id, id)

			cursor.execute(update_query)
			cnx.commit()

			print(" > Updated Source with id {}".format(self.aldfear_id))


def main():
	print(fix_dates("18-MAR-1930"))
	print(fix_dates("BEF-1930"))
	print(fix_dates("AFT-1930"))
	return 0

if __name__ == "__main__":
	main()