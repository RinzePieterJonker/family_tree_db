#!/usr/bin/env python3

import mysql.connector
import time
import configparser


from DataTypes import *

# GLOBALS
config = configparser.ConfigParser()
config.read("../config.ini")
USERNAME = config["credentials"]["username"]
PASSWORD = config["credentials"]["password"]
DATABASE = config["system"]["database"]
HOST = config["system"]["host"]
PORT = config["system"]["port"]
REPLACE = bool(int(config["system"]["replace"]))

FILE = "../SQL/FamilyTreeDB.sql"

# CODE
def create_connection():
	try:
		cnx = mysql.connector.connect(user=USERNAME,
									  password=PASSWORD,
									  database=DATABASE,
									  host=HOST)
		cursor = cnx.cursor()

		print(" > Connection has been made to the database")
	except:
		# Add error handling
		print(" > No connection has been made")
		pass
	return cursor, cnx


def create_database(cursor, cnx):
	drop_tables = [
		"drop table if exists Sources",
		"drop table if exists Notes",
		"drop table if exists Relationships",
		"drop table if exists Families",
		"drop table if exists Individuals"
	]

	for query in drop_tables:
		cursor.execute(query)
		cnx.commit()

	create_tables = [
		"create database if not exists familytreedb",

		"use familytreedb",

		"create table Individuals("
		"id						int				auto_increment,"
		"individual_id			varchar(255)	not null,"
		"first_name 			varchar(255),"
		"insertion 				varchar(10),"
		"last_name 				varchar(255),"
		"gender					varchar(100),"
		"occupations 			varchar(255),"
		"residences				varchar(255),"
		"birth_date 			varchar(255) 	not null,"
		"birth_year				varchar(255),"
		"birth_place 			varchar(255),"
		"death_date				varchar(255)	not null,"
		"death_year				varchar(255),"
		"death_place 			varchar(255),"
		"crem_burried			varchar(255)	not null,"
		"crem_burried_date 		varchar(255),"
		"crem_burried_place 	varchar(255),"
		"birth_fam_id 			varchar(255),"
		"married_fam_id 		varchar(255),"
		"img_path				varchar(255),"
		"events					varchar(255),"

		"primary key (id),"
		"index (married_fam_id, individual_id));",

		"create table Families("
		"id				int				auto_increment,"
		"family_id	 	varchar(255)	not null,"
		"head_of_family varchar(255)	not null,"
		"husband		varchar(255),"
		"wife			varchar(255),"
    	"insertion		varchar(255),"
    	"last_name		varchar(255),"
    	"primary key (id),"
    	"foreign key (family_id) references Individuals(married_fam_id));",

		"create table Relationships("
		"relationship_id		int				auto_increment	not null,"
    	"family_id				varchar(255) 	not null,"
    	"individual_1_id		varchar(255)	not null,"
    	"individual_2_id		varchar(255)	not null,"
		"individual_1_db_id		int,"
		"individual_2_db_id		int,"
    	"individual_1_role	varchar(255),"
    	"individual_2_role	varchar(255),"
    	"relationship_type	varchar(255),"
    	"relationship_date	varchar(255),"
    	"relationship_place	varchar(255),"

		"primary key (relationship_id),"
    	# "foreign key (family_id) references Families(family_id),"
        "foreign key (individual_2_db_id) references Individuals(id));",

		"create table Notes("
		"id					int		auto_increment,"
		"individual_id		int,"
		"relationship_id	int,"
		"note_id			varchar(255)	not null,"
   		"about				varchar(255),"
		"note				varchar(255),"

		"primary key(id),"
		"foreign key(individual_id) references Individuals(id),"
		"foreign key(relationship_id) references Relationships(relationship_id));",

		"create table Sources("
		"id					int		auto_increment,"
		"individual_id		int,"
		"relationship_id	int,"
		"source_id			varchar(255),"
		"source_type		varchar(255),"
		"source				varchar(255),"

		"primary key(id),"
		"foreign key(individual_id) references Individuals(id),"
		"foreign key(relationship_id) references Relationships(relationship_id));"
	]

	for query in create_tables:
		cursor.execute(query)
		cnx.commit()


def add_final(cursor, cnx):

	ids = ["@R1@", "@R2@", "@R3@", "@R4@", "@R5@"]
	queries = [
		"insert into Individuals(individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, birth_place, death_date, crem_burried, birth_fam_id, married_fam_id, img_path) "
		"values ('@R1@', 'Dieuwke', 'van der', 'Schaaf', 'female', '2 november 1963', '1963', 'Witmarsum', '', '', '@F1553363765@', '@RF1@', 'Dieuwke_Jonker_van_der_Schaaf.jpg | Trouwfoto_Heit_Mem.jpg')",
		"insert into Individuals(individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, birth_place, death_date, crem_burried, birth_fam_id, married_fam_id, img_path) "
		"values ('@R2@', 'Hendricus Jan', '', 'Jonker', 'male', '3 juli 1964', '1964', '', '', '', '@RF2@', '@RF1@', 'Hendricus_Jonker.jpg | Trouwfoto_Heit_Mem.jpg')",
		"insert into Individuals(individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, birth_place, death_date, crem_burried, birth_fam_id, married_fam_id, img_path) "
		"values ('@R3@', 'Rinze-Pieter', '', 'Jonker', 'male', '12 mei 1998', '1998', 'Heerenveen (Tsjongerschans)', '', '', '', '@RF1@', 'graduation_cut.jpg')",
		"insert into Individuals(individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, birth_place, death_date, crem_burried, birth_fam_id, married_fam_id) "
		"values ('@R4@', 'Anneke', '', 'Jonker', 'female', '20 oktober 1996', '1996', 'Heerenveen (Tsjongerschans)', '', '', '@F1483623052@', '@RF1@')",

		"insert into Individuals(individual_id, first_name, insertion, last_name, gender, birth_date, birth_year, birth_place, death_date, crem_burried, birth_fam_id, married_fam_id) "
		"values ('@R5@', 'Anne Pieters', '', 'Bok', 'female', '14 februari 1933', '1933', '', '', '', '', '@RF2@')"
	]

	for count, query in enumerate(queries):
		cursor.execute(query)
		cnx.commit()

		cursor.execute("select id from individuals where individual_id = '{}'".format(ids[count]))
		ids[count] = cursor.fetchall()[0][0]

	# finding parents of Dieuwke Jonnker-van der schaaf:
	cursor.execute("select id from individuals where individual_id = '@I5@' and last_name = 'Schaaf'")
	rinze = cursor.fetchall()[0][0]

	cursor.execute("select id from individuals where individual_id = '@I6@' and last_name = 'Hoekstra'")
	pietje = cursor.fetchall()[0][0]

	cursor.execute("select id from individuals where individual_id = '@I34@' and last_name = 'Bok'")
	wiege = cursor.fetchall()[0][0]

	cursor.execute("select id from individuals where individual_id = '@I35@' and last_name = 'Kalsbeek'")
	bontje = cursor.fetchall()[0][0]

	# Adding relations between the added persons
	queries = [
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type, relationship_date)"
		"values('@RF1@', '@R1@', '@R2@', {}, {}, 'wife', 'husband', 'marriage', '12 mei 1995')".format(ids[0],
																										  ids[1]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type, relationship_date)"
		"values('@RF1@', '@R2@', '@R1@', {}, {}, 'husband', 'wife', 'marriage', '12 mei 1995')".format(ids[1],
																										  ids[0]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF1@', '@R1@', '@R3@', {}, {}, 'mother', 'child', 'parental')".format(ids[0], ids[2]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF1@', '@R2@', '@R3@', {}, {}, 'father', 'child', 'parental')".format(ids[1], ids[2]),

		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF1@', '@R1@', '@R4@', {}, {}, 'mother', 'child', 'parental')".format(ids[0], ids[3]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF1@', '@R2@', '@R4@', {}, {}, 'father', 'child', 'parental')".format(ids[1], ids[3]),

		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@F1553363765@', '@I5@', '@R1@', {}, {}, 'father', 'child', 'parental')".format(rinze, ids[0]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@F1553363765@', '@I6@', '@R1@', {}, {}, 'mother', 'child', 'parental')".format(pietje, ids[0]),

		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@F1483623052@', '@I34@', '@R5@', {}, {}, 'father', 'child', 'parental')".format(wiege, ids[4]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@F1483623052@', '@I35@', '@R5@', {}, {}, 'mother', 'child', 'parental')".format(bontje, ids[4])
	]

	# @F1483623052@

	for count, query in enumerate(queries):
		cursor.execute(query)
		cnx.commit()

	# finding the dad of Hendricus Jonker
	cursor.execute("select id from individuals where individual_id = '@I5@' and last_name = 'Jonker'")
	jan = cursor.fetchall()[0][0]

	queries = [
		"update individuals set married_fam_id = '@RF2@' where id = {}".format(jan),

		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF2@', '@I5@', '@R2@', {}, {}, 'father', 'child', 'parental')".format(jan, ids[1]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF2@', '@T5@', '@R2@', {}, {}, 'mother', 'child', 'parental')".format(ids[4], ids[1]),

		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF2@', '@I5@', '@R5@', {}, {}, 'husband', 'wife', 'marriage')".format(jan, ids[4]),
		"insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_db_id, individual_2_db_id, individual_1_role, individual_2_role, relationship_type)"
		"values('@RF2@', '@R5@', '@I5@', {}, {}, 'wife', 'husband', 'marriage')".format(ids[4], jan)
	]

	for count, query in enumerate(queries):
		cursor.execute(query)
		cnx.commit()


def main():
	start = time.time()

	print(" > Started code at {}".format(time.ctime()))
	cursor, cnx = create_connection()

	files = ["../Resources/stamboom_hendricus.ged", "../Resources/stamboom_Dieuwke.ged"]
	# files = ["../Resources/stamboom_Dieuwke.ged"]
	# files = ["../Resources/stamboom_hendricus.ged"]

	create_database(cursor, cnx)

	current_object = None

	for file in files:
		with open(file, "r") as in_file:
			for line_count, line in enumerate(in_file):
				if line_count != 0:

					line = line.strip("\n").split(" ")

					if line[0] == "0":
						if current_object:
							if type(current_object) == list:
								for item in current_object:
									item.insert_data(cursor, cnx)
							else:
								current_object.insert_data(cursor, cnx)

							current_object = None

						current_type = line[-1]
						if current_type == "INDI":
							current_object = Person(aldfear_id = line[1])
						elif current_type == "FAM":
							current_object = [Family(aldfear_id = line[1]), RelationShips(aldfear_id = line[1])]
						elif current_type == "NOTE":
							current_object = Notes(aldfear_id = line[1])
						elif current_type == "SOUR":
							current_object = Sources(aldfear_id=line[1])


					if current_object:
						if type(current_object) == list:
							for item in current_object:
								item.add_data(line)
						else:
							current_object.add_data(line)

	add_final(cursor, cnx)

	cursor.close()
	cnx.close()

	print(" > Finished code at {}, took {} seconds".format(time.ctime(), round(time.time() - start)))
	return 0

if __name__ == "__main__":
	main()