#!/usr/bin/env python3

"""

"""

# METADATA
__author__ = "Rinze-Pieter Jonker"
__status__ = "Work in progress"
__version__ = 0.1


# IMPORTS
import configparser
import mysql.connector
import os

from GedFileEntry import create_connection


# GLOBALS
config = configparser.ConfigParser()
config.read("../config.ini")
USERNAME = config["credentials"]["username"]
PASSWORD = config["credentials"]["password"]
DATABASE = config["system"]["database"]
HOST = config["system"]["host"]
PORT = config["system"]["port"]
REPLACE = bool(int(config["system"]["replace"]))

FILE = "../SQL/FamilyTreeDB.sql"


# CODE
def extract_data(cursor, file_path = "../Resources/data.csv"):

	if os.path.exists(file_path):
		os.remove(file_path)

	cursor.execute("SELECT * FROM individuals")

	results = cursor.fetchall()

	with open(file_path, "w+") as file:

		file.write("# individual_id, first_name, insertion, last_name, gender, occupations, residences, birth_date,"
				   " birth_year, birth_place, death_date, death_year, death_place, crem_burried, crem_burried_date,"
				   " crem_burried_place, birth_fam_id, married_fam_id, img_path, events \n")

		for row in results:

			for count, item in enumerate(row):
				print(count)
				if item is None:
					file.write("")
				else:
					file.write(item)

				if count < 19:
					file.write(", ")
			file.write("\n")


def main():
	cursor, cnx = create_connection()

	extract_data(cursor)

	return 0


if __name__ == "__main__":
	main()
