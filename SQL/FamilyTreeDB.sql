create database if not exists familytreedb;
use familytreedb;

drop table if exists Relationships;
drop table if exists Families;
drop table if exists Notes;
drop table if exists Individuals;

create table Individuals(
    individual_id 			varchar(255) 	not null	 unique,
    first_name 				varchar(255),
    insertion 				varchar(10),
    last_name 				varchar(255),
    gender 					varchar(5),
    occupations 			varchar(255),
    occupations_dates 		varchar(255),
    birth_date 				varchar(255) 	not null,
    birth_place 			varchar(255),
    death_date				varchar(255)	not null,
    death_place 			varchar(255),
    crem_burried			varchar(255)	not null,
    crem_burried_date 		varchar(255),
    crem_burried_place 		varchar(255),
    birth_fam_id 			varchar(255),
    married_fam_id 			varchar(255),

    primary key (individual_id),
    index (birth_fam_id)
);

create table Families(
	family_id	 	varchar(255)	 not null,
    head_of_family	varchar(255)	 not null,
    
    primary key (family_id),
    
    index (head_of_family),
    
    foreign key (family_id) 
		references Individuals(birth_fam_id)
);

create table Relationships(
	relationship_id		int				auto_increment	not null,
    family_id			varchar(255) 	not null,
    individual_1_id		varchar(255)	not null,
    individual_2_id		varchar(255)	not null,
    individual_1_role	varchar(255),
    individual_2_role	varchar(255),
    relationship_type	varchar(255),
    relationship_start	varchar(255),
    relationship_end	varchar(255),
    relationship_place	varchar(255),
    
	primary key (relationship_id),
    
    foreign key (individual_1_id)
		references Families(head_of_family),
    foreign key (individual_2_id) 
		references Individuals(individual_id)
);

create table Notes(
	individual_id	varchar(255), 
    family_id		varchar(255),
    relationship_id	varchar(255),
    note_id			varchar(255)	not null,
    about			varchar(255),
    note			varchar(255),
    
    primary key(note_id),
    
    foreign key(individual_id)
		references Individuals(individual_id),
	foreign key(family_id)
		references Families(family_id)
);

-- insert into Individuals(individual_id, first_name, last_name, birth_date, death_date, crem_burried, crem_burried_date, birth_fam_id) 
-- 	values ("test_person", "test", "testerson", '2008-11-11', '2008-11-11', "crem",  '2008-11-11', "test_fam");
-- insert into Individuals(individual_id, first_name, last_name, birth_date, death_date, crem_burried, crem_burried_date, birth_fam_id) 
-- 	values ("test_person2", "test2", "testerson", '2008-11-11', '2008-11-11', "burried",  '2008-11-11', "test_fam");

-- insert into Families(family_id, head_of_family) 
-- 	values ("test_fam", "test_person");

-- insert into Relationships(family_id, individual_1_id, individual_2_id, individual_1_role, individual_2_role)
-- 	values ("test_fam", "test_person", "test_person2", "parrent", "child");

select * from individuals;
select * from families;
